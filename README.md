Parse da página de eventos do Mackenzie!

http://www3.mackenzie.com.br/eventos/eventos.php

**Necessário:** JSoup (http://jsoup.org/download) + Eclipse/Netbeans

* Tenha em mãos também um índice de evento inicial (http://www3.mackenzie.com.br/eventos/eventos2.php?evento=XX) onde XX é esse índice inicial.

* Execute e siga as instruções do console.

Criado por Caio Remedio e Giovanni Cornachini :)