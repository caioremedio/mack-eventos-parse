/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mack.eventos.parse;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;

import org.jsoup.HttpStatusException.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;

/**
 *
 * @author caioremedio
 */
public class MackEventosParse {

    public static void main(String[] args) throws IOException {

        String strTIA           = null;
        String strEventoInicio  = null;
        String strCategoriaFixed= null;
        
        int maxIndex = -1;
        
        Scanner tec = new Scanner(System.in);
        
        System.out.println("Digite seu TIA: ");
        while(strTIA == null || strTIA.length() == 0 || (strTIA.length() > 0 && strTIA.length() < 8)) {
            strTIA = tec.nextLine();
        }
        
        System.out.println("Digite o indice do evento que deseja começar: ");
        while(strEventoInicio == null || strEventoInicio.length() == 0) {
            strEventoInicio = tec.nextLine();
        }
        
        System.out.println("Digite o máximo de eventos que deseja obter: ");
        while(maxIndex == -1) {
            maxIndex = tec.nextInt();
        }
        
        System.out.println("Digite o nome da categoria que deseja filtrar. -1 para TODOS. ");
        while(strCategoriaFixed == null || strCategoriaFixed.length() == 0) {
            strCategoriaFixed = tec.nextLine().trim();
        }
        
        Connection.Response res = Jsoup.connect("http://www3.mackenzie.com.br/eventos/cadastro.php")
                .data("tipo", "1TIA", "TIA", strTIA, "evento", strEventoInicio)
                .method(Method.POST)
                .execute();

        Map<String, String> loginCookies = res.cookies();
        
        
        for (int i = 0, eventoIndex = Integer.parseInt(strEventoInicio); i < maxIndex; i++, eventoIndex++) {
            
            String strEventoURL = "http://www3.mackenzie.com.br/eventos/eventos2.php?evento=" + eventoIndex;
            Document doc2 = Jsoup.connect(strEventoURL)
                .cookies(loginCookies)
                .get();
            String strEventoCat = doc2.select("table[class=comdegrade]").select("p").first().toString();
            strEventoCat = removeTag("</strong>","</p>", strEventoCat).trim();
            
            if (!strCategoriaFixed.equalsIgnoreCase(strEventoCat) && !strCategoriaFixed.equals("-1")) {
                System.out.println("Pulando... " + strEventoCat);
                continue;
            }

            String strEventoNome = doc2.select("table[class=comdegrade]").select("p").get(1).text();
            String strEventoData = doc2.select("table[class=comdegrade]").select("p").get(2).text();
            String strEventoHorario = doc2.select("table[class=comdegrade]").select("p").get(4).text();
            
            FileWriter file = new FileWriter("../mack-eventos.txt", true);
            try {
                file.write("\nCategoria: " + strEventoCat + "\n" + strEventoNome + "\nURL: " + strEventoURL + "\n" + strEventoData + "\n" + strEventoHorario + "\n\n");
            } catch (IOException e) {
                    e.printStackTrace();

            } finally {
                file.flush();
                file.close();
            }
        }    
    }
    
    public static String removeTag(String firstTag, String lastTag, String value){
        int size = firstTag.length();
        
        value = value.substring(value.indexOf(firstTag)+size, value.indexOf(lastTag));
        
        return value;
    }
    
}
